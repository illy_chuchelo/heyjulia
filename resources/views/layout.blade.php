<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hey|Julia|Pht</title>
    <meta charset="UTF-8">
    <meta name="description" content="Hey|Julia|Pht">
    <meta name="keywords" content="portfolio, riddle, onepage, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,600i,700" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="/css/front.css"/>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="body-pad">


<!-- header section start -->
<header class="header-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-3">
                <div class="logo">
                    <h2 class="site-logo">HeyJuliaPht</h2>
                </div>
            </div>
            <div class="col-lg-8 col-md-9">

                <nav class="main-menu">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="/about">About</a></li>
                        <li><a href="/work">Work</a></li>
                        <li><a href="/contact">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="nav-switch">
        <i class="fa fa-bars"></i>
    </div>
</header>
<!-- header section end -->


<!-- portfolio section start -->
@yield('content')
<!-- portfolio section end -->


<!-- footer section start -->
<footer class="footer-section text-center">
    <div class="container">

        <div class="social-links">

            <a href=""><span class="fa fa-instagram"></span></a>
            <a href=""><span class="fa fa-vk"></span></a>
            <a href=""><span class="fa fa-twitter"></span></a>
            <a href=""><span class="fa fa-facebook"></span></a>
        </div>
        <div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </div>
    </div>
</footer>
<!-- footer section end -->



<!--====== Javascripts & Jquery ======-->
<script src="/js/front.js"></script>
</body>
</html>