<!DOCTYPE html>
<html lang="en">
<head>
    <title>Riddle - Portfolio Template</title>
    <meta charset="UTF-8">
    <meta name="description" content="Riddle - Portfolio Template">
    <meta name="keywords" content="portfolio, riddle, onepage, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,600i,700" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="/css/front.css"/>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="body-pad">
    <section class="page-section pt100">
        <!--main content start-->
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">

                        <div class="leave-comment mr0"><!--leave comment-->
                            @if(session('status'))
                                <div class="alert alert-danger">
                                    {{session('status')}}
                                </div>
                            @endif
                            <h3 class="text-uppercase">Login</h3>
                            @include('admin.errors')
                            <br>
                            <form class="form-horizontal contact-form" role="form" method="post" action="/login">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" id="email" name="email" value="{{old('email')}}"
                                               placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="password" class="form-control" id="password" name="password"
                                               placeholder="password">
                                    </div>
                                </div>
                                <button type="submit" class="btn send-btn">Login</button>

                            </form>
                        </div><!--end leave comment-->
                    </div>

                </div>
            </div>
        </div>
    </section>
    <script src="/js/front.js"></script>
</body>
</html>
