@extends('layout')

@section('content')
    <!-- page section start -->
    <section class="page-section pt100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 contect-tect">
                    <h2>Давай поработаем вместе?</h2>
                    <p>>одиночный фотосет (S) - 150 грн 20-30 обработанных фотографий+ все удачные оригиналы</p>
                    <p>одиночный фотосет (XS)- 100 грн 10-15 фото обработанных фотографий+ все удачные оригиналы</p>
                    <p>>фотосет с другом подругой-100 грн (с человека)</p>
                    <p>>фотосет влюбленных- 200грн</p>
                    <p>>репортаж- 150 грн в час</p>
                     <p>сертификат на любой вкус и цвет (цена договорная) </p>
                </div>
            </div>
            @if(session('status'))
                <div class="alert alert-danger">
                    {{session('status')}}
                </div>
            @endif
            @include('admin.errors')
            {!! Form::open(['url' => '/send', 'class' => 'contact-form']) !!}

            <div class="row">
                <div class="col-md-6">
                    <input type="text" name="name" placeholder="Как ваз зовут?">
                </div>
                <div class="col-md-6">
                    <input type="text" name="email" placeholder="E-mail">
                </div>



                <div class="col-md-12">
                    <textarea name="text" placeholder="Сообщение"></textarea>
                </div>
            </div>
            <div class="text-center">
                <button class="site-btn">Send</button>
            </div>

            {!! Form::close() !!}
        </div>
    </section>
    <!-- page section end -->

@endsection