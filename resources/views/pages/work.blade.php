@extends('layout')

@section('content')
    <!-- Page Preloder -->


    <section class="portfolio-section pt100">
        <div class="container">

        </div>
        <div class="container-fluid p-0">
            <div class="row portfolios-area">
                <div class="col-lg-6">
                    <div class="portfolio-intro">
                        <h2 class="section-title mb-5"><i>Это мои работы</i> </h2>
                        <ul class="portfolio-filter controls">
                            <li class="control" data-filter="all">All</li>
                            @foreach($categories as $category)
                                <li class="control" data-filter=".{{$category->slug}}">{{$category->title}}</li>
                            @endforeach

                        </ul>
                    </div>
                </div>
                <div class="first-item col-md-6 col-lg-12 mix {{$firstPost->getCategorySlug()}} p-lg-0">
                    <a href="{{$firstPost->getImage()}}" class="portfolio-item set-bg " data-setbg="{{$firstPost->getImage()}}">
                        <div class="pi-inner">
                            <h2>{{$firstPost->title}}</h2>
                        </div>
                    </a>
                    <div class="portfolio-meta">
                        <h2>{{$firstPost->description}}</h2>
                        @if($firstPost->inst != null)
                            <p>inst: <i>{{$firstPost->inst}}</i></p>
                        @endif
                    </div>
                </div>
                <div class="second-item mix col-md-6 {{$secondPost->getCategorySlug()}}">
                    <a href="{{$secondPost->getImage()}}" class="portfolio-item set-bg" data-setbg="{{$secondPost->getImage()}}">
                        <div class="pi-inner">
                            <h2>{{$secondPost->title}}</h2>
                        </div>
                    </a>
                    <div class="portfolio-meta">
                        <h2>{{$secondPost->description}}</h2>
                        @if($secondPost->inst != null)
                            <p>inst: <i>{{$secondPost->inst}}</i></p>
                        @endif
                    </div>
                </div>
                @foreach($posts as $post)
                    <div class="mix col-md-6 col-lg-3 {{$post->getCategorySlug()}}">
                 <a href="{{$post->getImage()}}" class="portfolio-item pi-style2 set-bg" data-setbg="{{$post->getImage()}}">
                         <div class="pi-inner">
                             <h2>{{$post->title}}</h2>
                         </div>
                 </a>
                   <div class="portfolio-meta">
                    <h2>{{$post->description}}</h2>
                       @if($post->inst != null)
                           <p>inst: <i>{{$post->inst}}</i></p>
                       @endif
                   </div>
                 </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection