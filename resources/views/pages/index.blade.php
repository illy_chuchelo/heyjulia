@extends('layout')

@section('content')
    <!-- Page Preloder -->

    <!-- intro section start -->
    <section class="intro-section">
        <div class="container text-center">
            <div class="row">
                <div class="col-xl-20 offset-xl-1">
                    <h2 class="section-title"><i>Ваш дружелюбный фотограф</i></h2>
                </div>
            </div>
        </div>
    </section>
    <!-- intro section end -->

    <section class="portfolio-section">

        <div class="container-fluid p-md-0 ">
            <div class="row portfolios-area">


                <div class="mix col-lg-6 col-md-6">
                    <a href="{{$firstPost->getImage()}}" class="portfolio-item set-bg" data-setbg="{{$firstPost->getImage()}}">
                        <div class="pi-inner">
                            <h2>{{$firstPost->title}}</h2>
                        </div>
                    </a>
                </div>
                <div class="mix col-lg-6 col-md-6">
                    <a href="{{$secondPost->getImage()}}" class="portfolio-item set-bg" data-setbg="{{$secondPost->getImage()}}">
                        <div class="pi-inner">
                            <h2>{{$secondPost->title}}</h2>
                        </div>
                    </a>
                </div>

                @foreach($posts as $post)
                    <div class="mix col-lg-4 col-md-6">
                        <a href="{{$post->getImage()}}" class="portfolio-item set-bg" data-setbg="{{$post->getImage()}}">
                            <div class="pi-inner">
                                <h2>{{$post->title}}</h2>
                            </div>
                        </a>
                    </div>
                @endforeach

            </div>
        </div>
    </section>
@endsection