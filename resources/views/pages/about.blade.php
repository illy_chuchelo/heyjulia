@extends('layout')

@section('content')

    <section class="page-section pt100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <p> я Юля, фотографию, буду рада сотрудничать со всеми-всеми<br>
                        Юля любит , когда:<br>
                        -проявляют креативность<br>
                        -не стесняются перед камерой<br>
                        -слушаются Юлю<br>
                        -разговаривают с ней<br>
                        -пишут имя фотограф в описании под фото<br><br>

                        Юля НЕ любит, когда:<br>
                        -опаздывают на фотосет.<br>
                        -спрашивают сколько стоит фотоаппарат.<br>
                        -напоминают за фото! ЮЛЯ ВСЕГДА ВСЕ ПОМНИТ<br>
                        -переносят или отменяют фотосет без причины<br><br>

                        буду рада сотрудничать с каждым! всем любви и печенья </p>

                </div>
                <div class="col-lg-5 offset-lg-1">
                    <figure class="pic-frame">
                        <img src="img/about.jpg" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </section>

@endsection