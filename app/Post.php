<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    protected $fillable = ['title', 'description', 'inst'];//поля для заполнения

    //protected $appends = ['category'];

    public function category()
    {
        return $this->belongsTo(Category::class );
    }

    public static function add($fields)//добавление поста
    {
        $post = new static;
        $post->fill($fields);          //заполняет поля из $fillable
        $post->save();

        return $post;
    }

    public function edit($fields)      //изменить пост
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove()           //удалить пост
    {
        $this->removeImage();          //удаление картинки
        $this->delete();

    }

    public function uploadImage($image)
    {
        if($image == null) {return;}

        $this->removeImage();

        $filename = str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads', $filename);
        $this->image = $filename;
        $this->save();
    }

    public function removeImage()
    {
        if($this->image != null)
        {
            Storage::delete('uploads/' . $this->image);
        }
    }

    public function getImage()
    {
        if($this->image == null)
        {
            return '/img/no-image.png';
        }
        return '/uploads/' . $this->image;
    }

    public function setCategory($id)
    {
        if($id == null){return;}

        $this->category_id = $id;
        $this->save();
    }

    public function setHome()
    {
        $this->in_work = 0;
        $this->save();
    }

    public function setWork()
    {
        $this->in_work = 1;
        $this->save();
    }

    public function toggleWork($value)
    {
        if($value == null)
        {
            return $this->setHome();
        }

        return $this->setWork();
    }

    public function getCategoryTitle()
    {
        return ($this->category != null)
            ?   $this->category->title
            :   'Нет категории';
    }

    public function getCategorySlug()
    {
        return $this->category != null ? $this->category->slug : null;
    }

    public function getCategoryID()
    {
        return ($this->category != null)
            ? $this->category->id
            : null;
    }

    public function getSlugTitle()
    {
        return $this->category->slug;
    }



    public static function getWorkPosts()
    {
        return self::where('in_work', 1)->orderBy('id', 'desc');
    }

    public static function getHomePosts()
    {
        return self::where('in_work', 0)->orderBy('id', 'desc');
    }

}

