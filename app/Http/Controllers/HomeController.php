<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $firstPost = Post::getHomePosts()->first();
        $secondPost = Post::getHomePosts()->where('id', '<', $firstPost->id)->first();
        $posts = Post::getHomePosts()->WhereNotIn('id', [$firstPost->id,$secondPost->id])->get();


        return view('pages.index', compact('posts',  'firstPost', 'secondPost'));
    }

    public function about()
    {
        return view('pages.about');
    }

    public function work()
    {
        $firstPost = Post::getWorkPosts()->first();
        $secondPost = Post::getWorkPosts()->where('id', '<', $firstPost->id)->first();
        $posts = Post::getWorkPosts()->WhereNotIn('id', [$firstPost->id,$secondPost->id])->get();
        $categories = Category::all()->WhereNotIn('title', 'Нет категории' );


        return view('pages.work', compact('posts',  'categories', 'firstPost', 'secondPost'));
    }


}
