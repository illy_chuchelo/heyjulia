<?php

namespace App\Http\Controllers;

use App\Mail\ContactEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function contact()
    {
        return view('emails.contact');
    }

    public function send(Request $request)
    {
        $this->validate($request,[

            'name' =>'required',
            'email' => 'required|email',

            'text' => 'required'
        ]);
        $name = $request->name;
        $email = $request->email;
        $text = $request->text;

        \Mail::to('test@$email.com')->send(new ContactEmail($name, $email, $text));
        return redirect()->back()->with('status','Сообщение отправлено ');
    }
}
